angular.module("aulaApp", ["ui.router"])
.config(function($stateProvider){
	$stateProvider
	.state("index", {
		url:"/",
		templateUrl: "index.html",
		controller: "IndexCtrl"
	})
	.state("formulario", {
		url:"/formulario",
		templateUrl: "formulario.html",
		controller: "FormularioCtrl"
	})
})
.service("ContatoService", function(){
	var vm = this;
	vm.contatos = [];
	vm.addContato = function(contato){
		vm.contatos.push(contato);
		console.info(vm.contatos);
	}
	vm.removerContato = function(contato){
		var idx = vm.contatos.indexOf(contato);
		vm.contatos.splice(idx, 1);
	}
	vm.getContatos = function(){
		return vm.contatos;
	}
})
.controller("IndexCtrl", function($scope, $state, ContatoService){
	$scope.titulo = "Aula Pos";
	$scope.contatos = [];
	
	$scope.getContatos = function(){
		//Ajax API rest 
		$scope.contatos = ContatoService.getContatos();
	}

	$scope.removerContato = function(contato){
		ContatoService.removerContato(contato);
		$scope.getContatos();
	}

	$scope.$watch(function(){
    	return $state.$current.name
	}, function(newVal, oldVal){
		console.info(newVal, oldVal);
		$scope.getContatos();
	}) 
})
.controller("FormularioCtrl" , function($scope, $state, ContatoService){
	$scope.contato = {};
	$scope.salvar = function(){
		var contato = angular.copy($scope.contato);
		ContatoService.addContato(contato);
		$state.go("index");
	}	
})

